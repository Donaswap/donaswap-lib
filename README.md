![Stars](https://img.shields.io/gitlab/stars/Donaswap/donaswap-lib?style=social)
![Forks](https://img.shields.io/gitlab/forks/Donaswap/donaswap-lib?style=social)
![Contributors](https://img.shields.io/gitlab/contributors/donaswap/donaswap-lib)
![Issues](https://img.shields.io/gitlab/issues/open/Donaswap/donaswap-lib)
![Merge requests](https://img.shields.io/gitlab/merge-requests/open/Donaswap/donaswap-lib)
![Last Commit](https://img.shields.io/gitlab/last-commit/Donaswap/donaswap-lib)
![Discord](https://img.shields.io/discord/851473572772970527?label=Discord)
![Twitter Follow](https://img.shields.io/twitter/follow/donaswap?style=social)
# Donaswap Lib

Solidity libraries that are shared across Donaswap contracts. This package focuses on safety and execution gas efficiency.


## Install

Run `yarn` to install dependencies.

## Test

Run `yarn test` to execute the test suite.

## Usage

Install this in another project via `yarn add @uniswap/lib` 

Then import the contracts via:

```solidity
import "@donaswap/donaswap-lib/contracts/access/Ownable.sol"; 
```
